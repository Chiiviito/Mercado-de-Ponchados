<?php

	namespace Controlador;
	use Sistema\FiltrarValores as F;
	use Sistema\Ruta as Ruta;

	// Modelos
	use Modelo\Usuario as Usuario;


	class SistemaControlador extends Controlador
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function verificarRuta($nombre)
		{
			$Ruta = new Ruta();
			$rutas = $Ruta->leerRutas();

			$rutas = json_decode($rutas);
			$estado = false;
			foreach ($rutas as $key => $value) {
				if($nombre == $key) {
					$controlador = $value;
					$estado = true;
				}
			}


			// Ejecuta la accion si existe la ruta

			if($estado) {
				echo json_encode($rutas);
			} else{ 
				http_response_code(404);
				echo 0;
			}

			//echo ($rutas);
		}

	}