<?php

	namespace Controlador;
	use Sistema\Flash 	as Flash;
	use Sistema\FiltrarValores as F;
	use Sistema\Validador as V;
	use Sistema\Enviador as Enviador;

	// Modelos
	use Modelo\Usuario as Usuario;
	use Modelo\Ponchado as Ponchado;
	use Modelo\Categoria as Categoria;
	use Modelo\Compra as Compra;
	use Modelo\Pagina as Pagina;
	use MP;


	class SitioControlador extends Controlador
	{
		public function __construct()
		{
			parent::__construct();
			$this->V = new V();
		}

		public function index()
		{
			/*$Categoria = new Categoria();
			$categorias = $Categoria->traer();

			$Producto = new Producto();
			$Producto->orden(['fecha_alta'], 'DESC');
			$productos = $Producto->traer();

			// Busca las imagenes
			$Imagen = new Imagen();
			for ($i=0; $i < count($productos); $i++) {
				$Imagen->condicion('WHERE', 'producto', '=', $productos[$i]['id']);
				$Imagen->orden(['fecha_alta'], 'DESC');
				$productos[$i]['imagenes'] = $Imagen->traer();
			}*/

			/*$mp = new MP ("6301579089442348", "lyALfQ00YmD2JqNVnCW2eZjbPVSt7if8");
			$this->gg($mp);


			$preference_data = array (
				"items" => array (
					array (
						"title" => "Test",
						"quantity" => 1,
						"currency_id" => "USD",
						"unit_price" => 10.4
					)
				)
			);

			$preference = $mp->create_preference($preference_data);

			$this->gg($preference);*/

			/*$this->render('inicio', [
				'categorias' => $categorias,
				'productos' => $productos
			]);*/

			// Condiciones
			//$Usuario->condicion('WHERE', 'nombre', '=', 'Roberto');
			//$Usuario->condicion('WHERE', 'dni', '=', '94248948');
			


			//ORDER
			//$Usuario->orden(['nombre', 'apellido', 'correo'], 'ASC');
			/*$Usuario->orden(['dni'], 'ASC');

			//$this->gg($Usuario->orden);

			$resultado = $Usuario->traer(2, null, 'pagina');
			$this->gg($resultado);*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			//$Character = new Character();
			//$Character->condicion('WHERE', 'nombre', '=', 'Roberto');
			//$Character->condicion('WHERE', 'AccountID', '=', 'axess19');
			//$Character->orden(['dni'], 'ASC');
			//$characters = $Character->traer(10, ['AccountID', 'Name']);
			//dd($characters);


			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/

			/*$Character = new Character();
			$Character->valores['nombre'] = 'Roberto';
			$Character->valores['apellido'] = 'Calani';
			$Character->valores['correo'] = 'robertocalani@outlook.com';

			dd($Character->insertar());*/

			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*$Character = new Character();
			$Character->valores['nombre'] = 'Roberto';
			$Character->valores['apellido'] = 'Calani2';
			$Character->valores['correo'] = 'robertocalani@outlook.com';
			$Character->condicion('WHERE', 'nombre', '=', 'Roberto');

			dd($Character->actualizar());*/


			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/

			/*$Character = new Character();
			$Character->condicion('WHERE', 'id', '=', '2');

			$Character->eliminar();*/


			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/
			/*-------------------------------------------------------------*/

			// $Character = new Character();
			// dd($Character->verificar('nombre', 'Robertos'));

		   
			//dd(F::leerVariblesGlobales('paises'));

			/*$Usuario = new Usuario();
			$Usuario->valores['correo'] = 'robertocalani@outlook.com';
			$Usuario->valores['clave'] = $Usuario->crearClave('123456');
			$Usuario->valores['tipo'] = 1;

			echo '<code>' . $Usuario->insertar() . '</code>';


			$resultado = $Usuario->traer(10, null, 'pagina');
			dd($resultado);*/
			// $Categoria = new Categoria();
			// $Categoria->orden(['titulo'], 'ASC');
			// $categorias = $Categoria->traer();

			// $Producto = new Producto();
			// $Producto->orden(['fecha_alta'], 'ASC');
			// $productos = $Producto->traer(12, null, 'pagina');
			// $pagina = $Producto->paginacion();
			//echo ruta('sitio.iqndex');
			//mensajeDeError('asd');
			$Categoria = new Categoria();
			$Categoria->orden(['nombre'], 'ASC');
			$categorias = $Categoria->traer();

			$Ponchado = new Ponchado();
			$Ponchado->orden(['fecha_alta'], 'DESC');
			$ponchados = $Ponchado->traer(16, null, 'pagina');
			$pagina = $Ponchado->paginacion();			

			$this->render('inicio', ['categorias'=>$categorias, 'ponchados'=>$ponchados, 'pagina'=>$pagina]);
		}
		
		public function boton()
		{
			/*$mp = new \MP ("6301579089442348", "lyALfQ00YmD2JqNVnCW2eZjbPVSt7if8");
			$access_token = $mp->get_access_token();
			dd($access_token);*/
			
			//------------------------------------------------------------------------------------------------------------------------------------------------
			/*
				https://www.mercadopago.com.ar/developers/es/solutions/payments/basic-checkout/receive-payments/additional-info/
				https://www.mercadopago.com.ar/developers/es/solutions/payments/basic-checkout/receive-payments/
			*/
			$url_back = 'http://' . $_SERVER['HTTP_HOST'] . ruta('sitio.getPayment');

			$mp = new \MP ("6301579089442348", "lyALfQ00YmD2JqNVnCW2eZjbPVSt7if8");
			$mp->sandbox_mode(true);

			$preference_data = [
				'items' => [
					[
						"title" => "Carga de saldo - MercadoDePonchados.com",
						"quantity" => 1,
						"currency_id" => "USD", // Available currencies at: https://api.mercadopago.com/currencies
						"unit_price" => 10.00
					]
				],
				/*'back_urls' => [
					'success' => $url_back,
					'pending' => $url_back,
					'failure' => $url_back
				],
				'external_reference' => 'robertocalani@outlook.com',
				'payer' => [
					'name'	=> 'Roberto Calani',
					'email'	=> 'robertocalani@outlook.com',
				],
				'auto_return' => 'all',*/
				'notification_url' => 'http://' . $_SERVER['HTTP_HOST'] . ruta('sitio.notification')
			];
			$preference = $mp->create_preference($preference_data);
			dd($preference);

			/*$paymentInfo = $mp->get_payment ($preference['response']['id']);

			dd($paymentInfo);*/
		}

		public function notification()
		{
			$nombre_archivo = variables('directorio') . '/' . time() . ".txt";

			if($archivo = fopen($nombre_archivo, "a")) {
				fwrite($archivo, $_GET['topic'] . '*' . $_GET['id']);
				fclose($archivo);
			}
		}

		public function encontrarPagina($nombre)
		{
			$Pagina = new Pagina();

			if($Pagina->verificar('nombre', $nombre) == 0) {
				$this->render('404');
			}

			$Pagina->condicion('WHERE', 'nombre', '=', $nombre);
			$pagina = $Pagina->traer(1);

			F::redireccionar(ruta('sitio.pagina', ['nombre'=>$nombre, 'titulo_url'=>$pagina['titulo_url']]));
		}

		public function pagina($nombre, $titulo_url)
		{
			$Pagina = new Pagina();

			$Pagina->condicion('WHERE', 'nombre', '=', $nombre);
			$pagina = $Pagina->traer(1);

			$this->render('pagina', ['pagina'=>$pagina]);
		}

		public function ponchado($id, $titulo_url)
		{
			$Ponchado = new Ponchado();
			
			if(!$Ponchado->verificar('id', $id)) {
				F::redireccionar(ruta('sitio.index'));
			}

			$Categoria = new Categoria();
			$Compra = new Compra();
			$ponchado = [];
			$Ponchado->condicion('WHERE', 'id', '=', $id);
			$ponchado = $Ponchado->traer(1);

			$Categoria->condicion('WHERE', 'id', '=', $ponchado['categoria']);
			$ponchado['categoria'] = $Categoria->traer(1);

			$Compra->condicion('WHERE', 'ponchado', '=', $id);
			$Compra->orden(['fecha_alta'], 'ASC');
			$compras = $Compra->traer();

			// Verificar si ya lo compro
			$Compra->condicion('WHERE', 'usuario', '=', F::traerConectado());
			$Compra->condicion('WHERE', 'ponchado', '=', $id);
			$compra = $Compra->traer(1);

			$foto = ($ponchado['foto3']) ? $ponchado['foto3'] : ($ponchado['foto2']) ? $ponchado['foto2'] : ($ponchado['foto1']) ? $ponchado['foto1'] : '';


			$this->render('ponchado', ['ponchado'=>$ponchado, 'compras'=>$compras, 'compra'=>$compra, 'foto'=>$foto]);	
		}

		public function comprar($id, $titulo_url)
		{
			$this->proteger();
			$Ponchado = new Ponchado();
			
			if(!$Ponchado->verificar('id', $id)) {
				F::redireccionar(ruta('sitio.index'));
			}

			$Usuario = new Usuario();
			$Ponchado->condicion('WHERE', 'id', '=', $id);
			$ponchado = $Ponchado->traer(1);

			$fotos = [];
			if($ponchado['foto1']) {
				$fotos []= $ponchado['foto1'];
			}
			if($ponchado['foto2']) {
				$fotos []= $ponchado['foto2'];
			}
			if($ponchado['foto3']) {
				$fotos []= $ponchado['foto3'];
			}

			if(count($fotos) > 0) {
				$foto = $fotos[0];
			}

			$Usuario = new Usuario();
			$Usuario->condicion('WHERE', 'id', '=', F::traerConectado());
			$usuario = $Usuario->traer(1);

			$this->render('comprar', ['ponchado'=>$ponchado, 'foto'=>$foto, 'usuario'=>$usuario]);	
		}

		public function enviarComprar($id, $titulo_url)
		{
			$this->proteger();
			$Ponchado = new Ponchado();
			
			if(!$Ponchado->verificar('id', $id)) {
				F::redireccionar(ruta('sitio.index'));
			}

			$Ponchado->condicion('WHERE', 'id', '=', $id);
			$ponchado = $Ponchado->traer(1);

			$Usuario = new Usuario();
			$Usuario->condicion('WHERE', 'id', '=', F::traerConectado());
			$usuario = $Usuario->traer(1);

			if($usuario['dinero_compra'] < $ponchado['precio']) {
				Flash::rojo('No tienes dinero suficiente para poder comprar este ponchado<br>Puedes cargar dinero desde aqui: <a href="#">Cargar dinero</a>');
				F::redireccionar(ruta('sitio.ponchado', ['id'=>$id,'titulo_url'=>$titulo_url]));
			}

			$Compra = new Compra();

			// Verificar si ya lo compro
			$Compra->condicion('WHERE', 'usuario', '=', F::traerConectado());
			$Compra->condicion('WHERE', 'ponchado', '=', $id);

			if(count($Compra->traer()) > 0) {
				Flash::rojo('Ya se compro este ponchado');
				F::redireccionar(ruta('sitio.ponchado', ['id'=>$id,'titulo_url'=>$titulo_url]));
			} 

			// Actualizar numero de compras del ponchado
			$Ponchado->condicion('WHERE', 'id', '=', $id);
			$Ponchado->valores['compras'] = $ponchado['compras'] + 1;
			$Ponchado->actualizar();

			$Compra->valores['usuario'] = F::traerConectado();
			$Compra->valores['ponchado'] = $id;
			$Compra->valores['precio'] = $ponchado['precio'];

			if($Compra->insertar()) {
				$Usuario->condicion('WHERE', 'id', '=', F::traerConectado());
				$Usuario->valores['dinero_compra'] = $usuario['dinero_compra'] - $ponchado['precio'];
				if($Usuario->actualizar()) {
					Flash::verde('<strong>&iexcl;Felicidades!</strong> Ponchado comprado con exito.');
					F::redireccionar(ruta('sitio.ponchado', ['id'=>$id,'titulo_url'=>$titulo_url]));
				}
			}
		}

		private function descargarError() {
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=Ponchado.txt');
			header('Pragma: no-cache');
			readfile(variables('directorio') . '/error.txt');
		}

		private function descargarPonchado($ponchado, $nombre) {
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . $nombre);
			header('Pragma: no-cache');
			readfile(variables('directorio') . '/ponchados/' . $ponchado . '.broke');
		}

		public function descargar($id, $tipo) {	
			
			$campo = F::cargador();

			// Primero carga el ponchado antes que nada
			$Ponchado = new Ponchado();
			
			if(!$Ponchado->verificar('id', $id)) {
				$this->descargarError();
				return;
			}

			$Ponchado->condicion('WHERE', 'id', '=', $id);
			$ponchado = $Ponchado->traer(1);



			if($ponchado['gratis'] == 0) {
				$Compra = new Compra();

				// Verificar si ya lo compro
				$Compra->condicion('WHERE', 'usuario', '=', F::traerConectado());
				$Compra->condicion('WHERE', 'ponchado', '=', $id);

				/*$datos = [
					'tipo' => @$campo['tipo'],
					'id' => $campo['id'],
					'href' => '',
					'download' => '',
					'estado' => 0
				];*/

				if(count($Compra->traer()) == 0) {
					$this->descargarError();
					return;
				}	
			}

			if($tipo == 1) {
				$this->descargarPonchado($ponchado['ponchado1'], $ponchado['nombre'] . '.dst');
				return;
			}

			if($tipo == 2) {
				$this->descargarPonchado($ponchado['ponchado2'], $ponchado['nombre'] . '.jef');
				return;
			}

			if($tipo == 3) {
				$this->descargarPonchado($ponchado['ponchado3'], $ponchado['nombre'] . '.pes');
				return;
			}
		}

		/*public function eliminar()
		{
			$id = $_POST['id'];
			$tipo = $_POST['tipo'];

			$Ponchado = new Ponchado();
			
			if(!$Ponchado->verificar('id', $id)) {
				exit;
			}

			$Ponchado->condicion('WHERE', 'id', '=', $id);
			$ponchado = $Ponchado->traer(1);

			if($tipo == 1) {
				unlink(variables('directorio') . '/tmp/' . md5(F::traerConectado()) . '.MercadoDeMatrices');
			}

			if($tipo == 2) {
				unlink(variables('directorio') . '/tmp/' . md5($ponchado['ponchado2'] . $ponchado['id']) . '.MercadoDeMatrices');
			}

			if($tipo == 3) {
				unlink(variables('directorio') . '/tmp/' . md5($ponchado['ponchado3'] . $ponchado['id']) . '.MercadoDeMatrices');
			}			
		}*/

		public function gratis()
		{
			$Ponchado = new Ponchado();
			$Ponchado->condicion('WHERE', 'gratis', '=', 1);
			$Ponchado->orden(['fecha_alta'], 'DESC');
			$ponchados = $Ponchado->traer(16, null, 'pagina');
			$pagina = $Ponchado->paginacion();

			$Categoria = new Categoria();
			$Categoria->orden(['fecha_alta'], 'ASC');
			$categorias = $Categoria->traer();


			$this->render('gratis', ['ponchados'=>$ponchados, 'categorias'=>$categorias, 'pagina'=>$pagina]);
		}

		public function categoria($id, $nombre_url)
		{
			$Categoria = new Categoria();
			if(!$Categoria->verificar('id', $id)) {
				F::redireccionar(ruta('sitio.index'));
			}

			$Categoria->condicion('WHERE', 'id', '=', $id);
			$categoria = $Categoria->traer(1);

			$Ponchado = new Ponchado();
			$Ponchado->condicion('WHERE', 'categoria', '=', $id);
			$Ponchado->orden(['fecha_alta'], 'DESC');
			$ponchados = $Ponchado->traer(16, null, 'pagina');
			$pagina = $Ponchado->paginacion();

			$Categoria->orden(['fecha_alta'], 'ASC');
			$categorias = $Categoria->traer();

			$Categoria->condicion('WHERE', 'id', '=', $id);
			$categoria = $Categoria->traer(1);

			$this->render('categoria', ['ponchados'=>$ponchados, 'categorias'=>$categorias, 'categoria'=>$categoria, 'pagina'=>$pagina]);
		}

		public function buscar()
		{
			if(@!$_GET['p']) F::redireccionar(ruta('sitio.index'));

			$string = str_replace('+',' ',$_GET['p']);

			$Categoria = new Categoria();
			$Categoria->orden(['nombre'], 'ASC');
			$categorias = $Categoria->traer();

			$Ponchado = new Ponchado();
			$Ponchado->condicion('WHERE', 'titulo', 'LIKE', '%' . $string . '%');

			$Ponchado->orden(['fecha_alta'], 'DESCgrat');
			$ponchados = $Ponchado->traer(16, null, 'pagina');
			$pagina = $Ponchado->paginacion();			

			$this->render('buscar', ['categorias'=>$categorias, 'ponchados'=>$ponchados, 'pagina'=>$pagina]);
		}

		public function ingresar()
		{
			$this->render('ingresar');
		}

		public function enviarIngresar()
		{
			$campo = F::cargador();

			$r = $this->V->validar([
				'correo'    => $campo['correo'] . "|requerido|correo",
				'clave'     => $campo['clave'] . "|requerido|min_6|max_20",
				'captcha'   =>  $_POST['g-recaptcha-response'] . "|captcha"
			]);
			
			if($r) {
				Flash::rojo($r);
				F::redireccionar(ruta('sitio.ingresar'));
			}

			$Usuario = new Usuario();

			if(!$Usuario->verificar('correo', $campo['correo'])) {
				Flash::rojo('El correo ingresado no existe');
				F::redireccionar(ruta('sitio.ingresar'));
			}

			// Trae los datos del usuario para luego comprar sus datos
			$Usuario->condicion('WHERE', 'id', '=', F::traerConectado());
			$u = $Usuario->traer(1);

			if(!$Usuario->verificarCadena($campo['correo'], $campo['clave'])) {
				Flash::rojo('La clave ingresada no coincide con la cuenta');
				F::redireccionar(ruta('sitio.ingresar'));
			}

			$Usuario->condicion('WHERE', 'correo', '=', $campo['correo']);
			$usuario = $Usuario->traer(1);

			if($usuario['validacion'] != 1) {
				Flash::azul('La cuenta solicitada no se encuentra activada, para volver a enviar el correo de validacion haga click en el siguiente enlace: <a href="' . ruta('sitio.reValidar') . '">[ Enviar correo de validacion ]</a>');
				F::redireccionar(ruta('sitio.ingresar'));
			}

			F::traerConectado($usuario['id']);
			F::redireccionar(ruta('sitio.index'));
		}

		public function recuperar()
		{
			$this->render('recuperar');
		}

		public function enviarRecuperar()
		{
			$nuevaClave = str_shuffle('azbycx09145');
			$Usuario = new Usuario();
			if(!$Usuario->verificar('correo', $_POST['correo'])) {
				Flash::rojo('El correo ingresado no esta registrado');
				F::redireccionar(ruta('sitio.recuperar'));
			}

			$r = $this->V->validar([
				'correo'    => $_POST['correo'] . "|requerido|correo",
				'captcha'   =>  $_POST['g-recaptcha-response'] . "|captcha"
			]);
			if($r) {
				Flash::rojo($r);
				F::redireccionar(ruta('sitio.recuperar'));
			}

			$Usuario->condicion('WHERE', 'correo', '=', $_POST['correo']);
			$Usuario->valores['clave'] = $Usuario->encriptarCadena($nuevaClave);

			if($Usuario->actualizar()) {
				$datos = [
					'de' => ['no-responder@mercadodeponchados.com', 'Mercado de Ponchados'],
					'para' => [[$_POST['correo']]],
					'titulo' => 'Nueva clave de acceso - Mercado de Ponchados',
					'mensajeHTML' => [
						'titulo' => 'Nueva clave de acceso',
						'cuerpo' => 'Tu nueva clave es: ' . $nuevaClave . ' , recorda cambiarla!'
					]
				];

				$Enviador = new Enviador($datos);
				if($Enviador->enviar()) {
					Flash::verde('Se envio el correo de validacion nuevamente al correo: ' . $usuario['correo']);
					F::redireccionar(ruta('sitio.recuperar'));
				}
			}
		}

		public function registro()
		{
			$this->render('registro');
		}

		public function enviarRegistro()
		{

			$campo = F::cargador();
			$r = $this->V->validar([
				'correo'    => $campo['correo'] . "|requerido|correo",
				'clave'     => $campo['clave'] . "|requerido|min_6|max_20",
				'confirmacion de clave'    => $campo['reclave'] . "|requerido|min_6|max_20",
				'captcha'   =>  $_POST['g-recaptcha-response'] . "|captcha"
			]);
			if($r) {
				Flash::rojo($r);
				F::redireccionar(ruta('sitio.registro'));
			}
			if($campo['clave'] !== $campo['reclave']) {
				Flash::rojo('Las claves no coinciden');
				F::redireccionar(ruta('sitio.registro'));
			}
			$Usuario = new Usuario();

			if($Usuario->verificar('correo', $campo['correo'])) {
				Flash::rojo('El correo ya esta registrado');
				F::redireccionar(ruta('sitio.registro'));
			}

			$Usuario->valores['nombre'] = F::html($campo['nombre']);
			$Usuario->valores['apellido'] = F::html($campo['apellido']);
			$Usuario->valores['correo'] = $campo['correo'];
			$Usuario->valores['clave'] = $Usuario->encriptarCadena($campo['clave']);
			$Usuario->valores['validacion'] = 1;
			
			if($Usuario->insertar()) {
				Flash::verde('<strong>Usuario creado correctamente!</strong> Ya puedes conectarte');
				F::redireccionar(ruta('sitio.ingresar'));
			}
		}

		public function reValidar()
		{
			$this->render('re_validar');
		}

		public function enviarReValidar()
		{
			$r = $this->V->validar([
				'correo'    => $_POST['correo'] . "|requerido|correo",
				'captcha'   =>  $_POST['g-recaptcha-response'] . "|captcha"
			]);
			if($r) {
				Flash::rojo($r);
				F::redireccionar(ruta('sitio.reValidar'));
			}

			$Usuario = new Usuario();

			if($Usuario->verificar('correo', $_POST['correo']) == 0) {
				Flash::azul('El correo ingesado no pertenece a ninguna cuenta');
				F::redireccionar(ruta('sitio.reValidar'));
			}

			$Usuario->condicion('WHERE', 'correo', '=', $_POST['correo']);
			$usuario = $Usuario->traer(1);

			if($usuario['validacion'] == 1) {
				Flash::azul('La cuenta ya se encuentra validada');
				F::redireccionar(ruta('sitio.reValidar'));
			}

			$datos = [
				'de' => ['no-responder@mercadodeponchados.com', 'Mercado de Ponchados'],
				'para' => [[$usuario['correo'], $usuario['nombre'] . ' ' . $usuario['apellido']]],
				'titulo' => 'Activacion de cuenta - Mercado de Ponchados',
				'mensajeHTML' => [
					'titulo' => 'Activacion de cuenta',
					'cuerpo' => 'Para poder activar tu cuenta haz click en el siguiente enlace: <br> <a href="' . variables('direccionSitio') . '/validar/' . $usuario['validacion'] . '" target="_blank">[ Validar cuenta ]</a>'
				]
			];

			$Enviador = new Enviador($datos);
			if($Enviador->enviar()) {
				Flash::verde('Se envio el correo de validacion nuevamente al correo: ' . $usuario['correo']);
				F::redireccionar(ruta('sitio.reValidar'));
			}
		}

		public function validarUsuario($codigo)
		{
			$Usuario = new Usuario();
			if($Usuario->verificar('validacion', $codigo) == 0){
				Flash::azul('El usuario ya se encuetra validado o el codigo de validacion no es correcto, si aun no puedes conectarte ingresa al siguiente enlace para volver a enviar el correo de validacion : <a href="' . ruta('sitio.reValidar') . '">[ Enviar correo de validacion ]</a>');
				F::redireccionar(ruta('sitio.ingresar'));
			}

			$Usuario->condicion('WHERE', 'validacion', '=', $codigo);
			$Usuario->valores['validacion'] = 1;

			if($Usuario->actualizar()){
				Flash::verde('<strong>Felicidades!</strong> Usuario validado correctamente, ahora puedes ingresar');
				F::redireccionar(ruta('sitio.ingresar'));
			}
		}

		public function admin()
        {
            if(!F::traerAdmin()) {
                $this->carpeta = '@admin';
                $this->render('ingresar');
            }
            $Admin = new AdminControlador();
            $Admin->index();
        }

        public function enviarIngresoAdmin()
        {
            $campo = F::cargador();
            $r = $this->V->validar([
                'correo'    => $campo['correo'] . "|requerido|correo",
                'clave'     => $campo['clave'] . "|requerido|min_6|max_20",
                'captcha'   =>  $_POST['g-recaptcha-response'] . "|captcha"
            ]);

            if($r) {
                Flash::rojo($r, 'admin');
                F::redireccionar(ruta('admin.index'));
            }
            $Usuario = new Usuario();

            $usuario = $Usuario->verificarCadena($campo['correo'], $campo['clave']);
            if(!$usuario) {
                Flash::rojo("El correo o clave ingresados son incorrectos", 'admin');
                F::redireccionar(ruta('admin.index'));
            }

            if($usuario['tipo'] == 0) {
            	Flash::rojo('El usuario ingresado no es administrador', 'admin');
                F::redireccionar(ruta('admin.index'));
            }

            unset($usuario['clave']);
            unset($_SESSION['conectadoAdmin']);
            F::traerAdmin(true);
            F::redireccionar(ruta('admin.index'));
        }

		/*-----------------------------------*/
		public function p404() {
			$this->render('404');
		}
		public function desconectar() {
            session_destroy();
            session_unset();
            session_regenerate_id(true);
            F::redireccionar(ruta('sitio.index'));
        }
	}