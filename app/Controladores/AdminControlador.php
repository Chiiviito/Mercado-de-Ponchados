<?php

	namespace Controlador;
	use Sistema\Flash 	as Flash;
	use Sistema\FiltrarValores as F;
	use Sistema\Validador as V;

	use Modelo\Categoria as Categoria;
	use Modelo\Ponchado as Ponchado;
	use Modelo\Usuario as Usuario;
	use Modelo\Pagina as Pagina;
	use Modelo\Ticket as Ticket;
	use Modelo\Compra as Compra;
	use Modelo\Saldo as Saldo;

	use upload;
	class AdminControlador extends Controlador
	{
		public $carpeta = '@admin';

		public function __construct() {
			parent::__construct();
			$this->protegerAdmin();
		}

		public function index()
		{
			$this->render('inicio');
		}

		public function paginas()
		{
			$pagina = [];
			$Pagina = new Pagina();
			$paginas = $Pagina->traer();

			// Traer categoria seleccionada
			if(@$_GET['id']) {
				$Pagina->condicion('WHERE', 'id', '=', $_GET['id']);
				$pagina = $Pagina->traer(1);
			}
			$this->render('paginas/paginas', ['paginas'=>$paginas, 'pagina' => $pagina]);
		}

		public function enviarPagina()
		{
			$campo = F::cargador();
			$Pagina = new Pagina();

			
			$Pagina->valores['titulo'] = F::html($campo['titulo']);
			$Pagina->valores['titulo_url'] = F::crearURL($campo['titulo']);
			$Pagina->valores['nombre'] = F::crearURL($campo['nombre']);
			$Pagina->valores['descripcion'] = F::html($campo['descripcion']);
			$Pagina->valores['contenido'] = $campo['contenido'];

			if(@$_GET['id']) {
				$Pagina->condicion('WHERE', 'id', '=', $_GET['id']);
				$Pagina->actualizar();
			} else {
				if($Pagina->verificar('nombre', F::crearURL($campo['nombre']))) {
					Flash::rojo('Ya existe una pagina con el mismo nombre', "flashAdmin");
					F::redireccionar(ruta('admin.paginas'));
				}
				$Pagina->insertar();
			}

			Flash::verde("Pagina agregada correctamente", "flashAdmin");
			F::redireccionar(ruta('admin.paginas'));
		}

		public function categorias()
		{
			$categoria = [];
			$Categoria = new Categoria();
			$categorias = $Categoria->traer(5, null, 'pagina');
			$pagina = $Categoria->paginacion();
			// Traer categoria seleccionada
			if(@$_GET['id']) {
				$Categoria->condicion('WHERE', 'id', '=', $_GET['id']);
				$categoria = $Categoria->traer(1);
			}
			$this->render('categorias/categorias', ['categorias'=>$categorias, 'categoria' => $categoria, 'pagina'=>$pagina]);
		}

		public function enviarCategoria()
		{
			$Categoria = new Categoria();

			$Categoria->valores['nombre'] = $_POST['nombre'];
			$Categoria->valores['nombre_url'] = F::crearURL($_POST['nombre']);

			if(@$_GET['id']) {
				$Categoria->condicion('WHERE', 'id', '=', $_GET['id']);
				$Categoria->actualizar();
			} else {
				$Categoria->insertar();
			}

			Flash::verde("Categoria agregada correctamente", "flashAdmin");
			F::redireccionar(ruta('admin.categorias'));
		}

		public function usuarios()
		{
			$Usuario = new Usuario();
			$usuarios = $Usuario->traer(10, null, 'pagina');
			$Usuario->orden(['fecha_alta'], 'DESC');
			$pagina = $Usuario->paginacion();
			$this->render('usuarios/usuarios', ['usuarios'=>$usuarios, 'pagina'=>$pagina]);
		}
		/**
		 *
		 * Usuarios
		 *
		 */
		
		public function agregarUsuario()
		{
			$this->render('usuarios/agregar');
		}

		public function enviarAgregarUsuario()
		{
			$campo = F::cargador();
			if(!$campo['nombre'] || !$campo['apellido'] || !$campo['correo'] || !$campo['clave']) {
				Flash::rojo("Faltan campos", "flashAdmin");
				F::redireccionar(ruta('admin.usuarios'));
			}
			$Usuario = new Usuario();

			$Usuario->valores['nombre'] = $campo['nombre'];
			$Usuario->valores['apellido'] = $campo['apellido'];
			$Usuario->valores['estado'] = $campo['estado'];
			$Usuario->valores['tipo'] = $campo['tipo'];
			$Usuario->valores['clave'] = $Usuario->encriptarCadena($campo['clave']);

			if($Usuario->verificar('correo', $campo['correo']) == 1) {
				Flash::rojo("El correo ingreso ya ", "flashAdmin");
				F::redireccionar(ruta('admin.usuarios'));
			} else {
				$Usuario->valores['correo'] = $campo['correo'];
			}
			if($id = $Usuario->insertar()) {
				Flash::verde("Usuario agregado correctamente", "flashAdmin");
				F::redireccionar(ruta('admin.usuarios') . '#' . $id);
			}
		}

		public function verUsuario($id)
		{
			$Usuario = new Usuario();
			$Direccion = new Direccion();
			$Direccion->condicion('WHERE', 'usuario', '=', $id);
			$direcciones = $Direccion->traer();

			$Usuario->condicion('WHERE', 'id', '=', $id); 
			$usuario = $Usuario->traer(1);
			$this->render('usuarios/detalles', ['usuario'=>$usuario, 'direcciones'=>$direcciones]);
		}

		public function enviarEditarUsuario($id)
		{
			$campo = F::cargador();

			if(!$campo['nombre'] || !$campo['apellido']) {
				Flash::rojo("Faltan campos", "flashAdmin");
				F::redireccionar(ruta('admin.usuarios'));
			}

			$Usuario = new Usuario();
			$Usuario->condicion('WHERE', 'id', '=', $id);

			$Usuario->valores['nombre'] = $campo['nombre'];
			$Usuario->valores['apellido'] = $campo['apellido'];
			$Usuario->valores['estado'] = $campo['estado'];
			$Usuario->valores['tipo'] = $campo['tipo'];
			$Usuario->valores['telefono'] = $campo['telefono'];
			$Usuario->valores['celular'] = $campo['celular'];
			$Usuario->valores['documento'] = $campo['documento'];
			$Usuario->valores['otros'] = $campo['otros'];

			if($campo['clave']) {
				$Usuario->valores['clave'] = $Usuario->encriptarCadena($campo['clave']);
			}

			if($Usuario->actualizar()) {
				Flash::verde("Usuario editado correctamente", "flashAdmin");
				F::redireccionar(ruta('admin.usuarios') . '#' . $id);
			}
		}

		public function eliminarUsuario($id)
		{
			$Usuario = new Usuario();
			$Usuario->condicion('WHERE', 'id', '=', $id);
			if($Usuario->eliminar()) {
				Flash::verde("Usuario eliminado correctamente", "flashAdmin");
				F::redireccionar(ruta('admin.usuarios'));
			}
		}

		public function verDireccion($usuario, $id)
		{
			$Direccion = new Direccion();
			$Direccion->condicion('WHERE', 'id', '=', $id);
			$direccion = $Direccion->traer(1);

			// Usuario
			$Usuario = new Usuario();
			$Usuario->condicion('WHERE', 'id', '=', $direccion['usuario']);
			$usuario = $Usuario->traer(1);

			$this->render('usuarios/direccion', ['direccion'=>$direccion, 'paises'=>variables('paises'), 'usuario'=>$usuario]);
		}

		public function enviarEditarDireccion($usuario, $id)
		{
			$campo = F::cargador();
			$Direccion = new Direccion();
			$Direccion->condicion('WHERE', 'id', '=', $id);
			$direccion = $Direccion->traer(1);

			//$Direccion->valores = $campo;
			$Direccion->valores = $campo;

			if($Direccion->actualizar($id)) {
				Flash::verde('Direccion editada correctamente', 'flashAdmin');
				F::redireccionar(ruta('admin.verDireccion', ['usuario'=>$direccion['usuario'], 'id'=>$direccion['id']]));
			}
		}

		/**
		 *
		 * Ponchados
		 *
		 */

		public function ponchados()
		{
			$ponchado = [];

			$Categoria = new Categoria();
			$categorias = $Categoria->traer();

			$Ponchado = new Ponchado();
			$Ponchado->orden(['fecha_alta'], 'DESC');
			$ponchados = $Ponchado->traer(10, null, 'pagina');

			if(@$_GET['id']) {
				$Ponchado->condicion('WHERE', 'id', '=', $_GET['id']);
				$ponchado = $Ponchado->traer(1);
			}

			$Categoria = new Categoria();
			for ($i=0; $i < count($ponchados); $i++) { 
				$Categoria->condicion('WHERE', 'id', '=', $ponchados[$i]['categoria']);
				$ponchados[$i]['categoria'] = $Categoria->traer(1);
			}

			$pagina = $Ponchado->paginacion();
			$this->render('ponchados/ponchados', ['ponchados'=>$ponchados, 'pagina'=>$pagina, 'categorias'=>$categorias, 'ponchado'=>$ponchado]);
		}

		public function enviarPonchado()
		{
			$campo = F::cargador();
			if(!$campo['titulo'] || !$campo['categoria'] || !$campo['nombre'] || !$campo['puntadas'] || !$campo['x'] || !$campo['y'] || !$campo['colores']) {
				Flash::rojo('Faltan campos', 'flashAdmin');
				F::redireccionar(ruta('admin.ponchados'));
			}
			$Ponchado = new Ponchado();
			unset($campo['color']);			

			// Rutas
			$rutaImagen = variables('directorio') . '/publico/img/ponchados/';
			$rutaPonchado = variables('directorio') . '/ponchados/';

			// Subir imagenes y archivos

			// Generar id
			$id = time();
			// Verifica si es gratis o no y le cambia el precio
			$campo['precio'] = ($campo['gratis'] == 1) ? 0 : $campo['precio'] ;

			$Ponchado->valores = $campo;
			$Ponchado->valores['titulo_url'] = F::crearURL($campo['titulo']);

			for ($i=1; $i < 4; $i++) {
				// Imagenes
				if(@$campo['foto' . $i]['tmp_name']) {
					$handle = new upload($_FILES['foto' . $i]);
					if ($handle->uploaded) {
						$handle->image_convert = 'jpg';
						$handle->file_new_name_ext = 'jpg';
						$handle->file_new_name_body   = $id . '_' . $i;
						$handle->image_watermark = variables('directorio') . '/publico/img/agua.png';
						$handle->image_watermark_position = 'BR';

						$handle->process($rutaImagen);
						if($handle->processed) {
							$Ponchado->valores['foto' . $i] = $id . '_' . $i;
							$handle->clean();
						}
						/*// Miniatura
						$handle = new upload($_FILES['foto' . $i]);
						$handle->file_new_name_body   = $id . '_mini_' . $i;
						$handle->image_convert = 'jpg';
						$handle->image_max_width = 200;
						$handle->image_max_height = 200;

						$handle->process($rutaImagen);
						if ($handle->processed) {
							$handle->clean();	
						}*/
					}
				}

				// Ponchados
				if(@$campo['ponchado' . $i]['tmp_name']) {
					$handle = new upload($_FILES['ponchado' . $i]);
					if ($handle->uploaded) {
						$handle->file_new_name_ext = 'broke';
						$handle->file_new_name_body   = $id . '_' . $i;
						$handle->process($rutaPonchado);
						if ($handle->processed) {
							$handle->clean();
							$Ponchado->valores['ponchado' . $i] = $id . '_' . $i;
						}
					}
				}
			}

			if(@$_GET['id']) {
				$Ponchado->condicion('WHERE', 'id', '=', $_GET['id']);
				if($Ponchado->actualizar()) {
					Flash::verde('El producto se actualizo correctamente', 'flashAdmin');
					F::redireccionar(ruta('admin.ponchados'));
				}
			} else {
				if($Ponchado->insertar()) {
					Flash::verde('El producto se agrego correctamente', 'flashAdmin');
					F::redireccionar(ruta('admin.ponchados'));
				}
			}
		}

		public function tickets()
		{
			$Ticket = new Ticket();
			$Ticket->condicion('WHERE', 'ticket', '=', 0);
			$Ticket->orden(['fecha_alta'], 'DESC');
			$tickets = $Ticket->traer(10, null, 'pagina');
			$pagina = $Ticket->paginacion();
			$this->render('tickets/tickets', ['tickets'=>$tickets]);
		}

		public function verTicket($id)
		{
			$Ticket = new Ticket();
			if($Ticket->verificar('id', $id) == 0) {
				F::redireccionar(ruta('admin.tickets'));
			}

			$Ticket->condicion('WHERE', 'id', '=', $id);
			$ticket = $Ticket->traer(1);

			$Ticket->condicion('WHERE', 'ticket', '=', $id);
			$Ticket->orden(['fecha_alta'], 'DESC');
			$respuestas = $Ticket->traer(5,null,'pagina');
			$pagina = $Ticket->paginacion();

			$this->render('tickets/ver_ticket', ['ticket'=>$ticket, 'respuestas'=>$respuestas, 'pagina'=>$pagina]);	
		}

		public function enviarMensajeTicket($id)
		{
			$campo = F::cargador();
			$V = new V();
			$r = $V->validar([
				'detalle'    => $campo['detalle'] . "|requerido|min_3|max_500"
			]);
			
			if($r) {
				Flash::rojo($r, 'flashAdmin');
				F::redireccionar(ruta('admin.verTicket', ['id'=>$id]));
			}
			$Ticket = new Ticket();

			$Ticket->condicion('WHERE', 'id', '=', $id);
			$Ticket->valores['respondido'] = 1;
			$Ticket->actualizar();

			$Ticket->valores['detalle'] = F::html($campo['detalle']);
			$Ticket->valores['usuario'] = F::traerConectado();
			$Ticket->valores['ticket'] = $id;

			if($Ticket->insertar()) {
				Flash::verde('Respuesta enviada con exito!', 'flashAdmin');
				F::redireccionar(ruta('admin.verTicket', ['id'=>$id]));
			}
		}

		public function compras()
		{
			$Compra = new Compra();
			$Compra->orden(['fecha_alta'], 'DESC');
			$compras = $Compra->traer();

			$Ponchado = new Ponchado();
			for ($i=0; $i < count($compras); $i++) { 
				$Ponchado->condicion('WHERE', 'id', '=', $compras[$i]['ponchado']);
				$compras[$i]['ponchado'] = $Ponchado->traer(1);

				if($compras[$i]['ponchado']['foto3']) $compras[$i]['ponchado']['foto'] = $compras[$i]['ponchado']['foto3'];
				if($compras[$i]['ponchado']['foto2']) $compras[$i]['ponchado']['foto'] = $compras[$i]['ponchado']['foto2'];
				if($compras[$i]['ponchado']['foto1']) $compras[$i]['ponchado']['foto'] = $compras[$i]['ponchado']['foto1'];
			}
			$this->render('usuarios/compras', ['compras'=>$compras]);
		}

		public function saldos()
		{
			$Usuario = new Usuario();
			$usuarios = $Usuario->traer();
			$this->render('usuarios/saldos', ['usuarios'=>$usuarios]);
		}

		public function enviarSaldo()
		{	
			$campo = F::cargador();
			$Saldo = new Saldo();
			$Saldo->valores['usuario'] = $campo['usuario'];
			$Saldo->valores['saldo'] = $campo['monto'];
			$Saldo->valores['medio'] = $campo['metodo'];

			if($Saldo->insertar()) {

				$Usuario = new Usuario();
				$Usuario->condicion('WHERE', 'id','=', $campo['usuario']);
				$usuario = $Usuario->traer(1);

				// -- 
				$Usuario->condicion('WHERE', 'id','=', $campo['usuario']);
				$Usuario->valores['dinero_compra'] = $usuario['dinero_compra'] + $campo['monto'];
				if($Usuario->actualizar()) {
					Flash::verde('Saldo cargado', 'flashAdmin');
					F::redireccionar(ruta('admin.saldos'));
				}
			}
		}
	}