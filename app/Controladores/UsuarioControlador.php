<?php

	namespace Controlador;
	use Sistema\Flash 	as Flash;
	use Sistema\FiltrarValores as F;
	use Sistema\Validador as V;
	use Sistema\Correo as Correo;

	// Modelos
	use Modelo\Ponchado as Ponchado;
	use Modelo\Usuario as Usuario;
	use Modelo\Compra as Compra;
	use Modelo\Saldo as Saldo;
	use Modelo\Ticket as Ticket;


	class UsuarioControlador extends Controlador
	{
		public function __construct()
		{
			parent::__construct();
			$this->proteger();
			$this->V = new V();
			$this->Usuario = new Usuario();
		}

		public function datos()
		{
			$Compra = new Compra();
			$Compra->condicion('WHERE', 'usuario', '=', F::traerConectado());
			$compras = count($Compra->traer());

			$Compra->condicion('WHERE', 'usuario', '=', F::traerConectado());
			$Compra->condicion('WHERE', 'comentario', '=', '');
			$sin_calificar = count($Compra->traer());

			$Usuario = new Usuario();
			$Usuario->condicion('WHERE', 'id', '=', F::traerConectado());
			$usuario = $Usuario->traer(1);
			$this->render('usuario/datos', ['usuario'=>$usuario, 'compras'=>$compras, 'sin_calificar'=>$sin_calificar]);
		}

		public function cargarSaldo()
		{
			$this->render('usuario/cargar');
		}

		public function historialSaldo()
		{
			$Saldo = new Saldo();
			$Saldo->condicion('WHERE', 'usuario', '=', F::traerConectado());
			$saldos = $Saldo->traer();
			$this->render('usuario/saldos', ['saldos'=>$saldos]);
		}

		public function enviarDatos()
		{
			$campo = F::cargador();
			$r = $this->V->validar([
				'nombre'    => $campo['nombre'] . "|requerido|min_3|max_60",
				'apellido'    => $campo['apellido'] . "|requerido|min_3|max_60",
				'captcha'   =>  $_POST['g-recaptcha-response'] . "|captcha"
			]);
			
			if($r) {
				Flash::rojo($r);
				F::redireccionar(ruta('usuario.datos'));
			}

			$Usuario = new Usuario();
			$Usuario->condicion('WHERE', 'id', '=', F::traerConectado());
			$Usuario->valores['nombre'] = $campo['nombre'];
			$Usuario->valores['apellido'] = $campo['apellido'];

			if($Usuario->actualizar()) {
				Flash::verde('Los datos fueron cambiados correctamente');
				F::redireccionar(ruta('usuario.datos'));
			}

		}

		public function cambiarClave()
		{
			$this->render('usuario/cambiar_clave');
		}

		public function enviarCambiarClave()
		{
			$campo = F::cargador();
			$r = $this->V->validar([
				'clave actual'    => $campo['claveactual'] . "|requerido|min_6|max_20",
				'clave nueva'    => $campo['clavenueva'] . "|requerido|min_6|max_20",
				'confirmacion clave nueva'    => $campo['reclavenueva'] . "|requerido|min_6|max_20",
				'captcha'   =>  $_POST['g-recaptcha-response'] . "|captcha"
			]);

			if($r) {
				Flash::rojo($r);
				F::redireccionar(ruta('usuario.cambiarClave'));
			}

			// Trae los datos del usuario para luego comprar sus datos
			$this->Usuario->condicion('WHERE', 'id', '=', F::traerConectado());
			$u = $this->Usuario->traer(1);

			if(!$this->Usuario->verificarCadena($u['correo'], $campo['claveactual'])) {
				Flash::rojo('La clave ingresada no coincide con la cuenta');
				F::redireccionar(ruta('usuario.cambiarClave'));
			}

			$this->Usuario->condicion('WHERE', 'id', '=', F::traerConectado());
			$this->Usuario->valores['clave'] = $this->Usuario->encriptarCadena($campo['clavenueva']);
			if($this->Usuario->actualizar()) {
				Flash::verde('La clave se cambio correctamente');
				F::redireccionar(ruta('usuario.cambiarClave'));
			}
		}

		public function compras()
		{
			$Compra = new Compra();
			$Compra->condicion('WHERE', 'usuario', '=', F::traerConectado());
			$Compra->orden(['fecha_alta'], 'DESC');
			$compras = $Compra->traer();

			$Ponchado = new Ponchado();
			for ($i=0; $i < count($compras); $i++) { 
				$Ponchado->condicion('WHERE', 'id', '=', $compras[$i]['ponchado']);
				$compras[$i]['ponchado'] = $Ponchado->traer(1);

				if($compras[$i]['ponchado']['foto3']) $compras[$i]['ponchado']['foto'] = $compras[$i]['ponchado']['foto3'];
				if($compras[$i]['ponchado']['foto2']) $compras[$i]['ponchado']['foto'] = $compras[$i]['ponchado']['foto2'];
				if($compras[$i]['ponchado']['foto1']) $compras[$i]['ponchado']['foto'] = $compras[$i]['ponchado']['foto1'];
			}
			$this->render('usuario/compras', ['compras'=>$compras]);
		}

		public function comprasSinCalificar()
		{
			$Compra = new Compra();
			$Compra->condicion('WHERE', 'usuario', '=', F::traerConectado());
			$Compra->condicion('WHERE', 'comentario', '=', '');
			$Compra->orden(['fecha_alta'], 'DESC');
			$compras = $Compra->traer();

			$Ponchado = new Ponchado();
			for ($i=0; $i < count($compras); $i++) { 
				$Ponchado->condicion('WHERE', 'id', '=', $compras[$i]['ponchado']);
				$compras[$i]['ponchado'] = $Ponchado->traer(1);

				if($compras[$i]['ponchado']['foto3']) $compras[$i]['ponchado']['foto'] = $compras[$i]['ponchado']['foto3'];
				if($compras[$i]['ponchado']['foto2']) $compras[$i]['ponchado']['foto'] = $compras[$i]['ponchado']['foto2'];
				if($compras[$i]['ponchado']['foto1']) $compras[$i]['ponchado']['foto'] = $compras[$i]['ponchado']['foto1'];
			}
			$this->render('usuario/compras', ['compras'=>$compras]);
		}

		public function calificar($id)
		{
			$Compra = new Compra();
			
			if(!$Compra->verificar('id', $id)) {
				Flash::rojo('La compra seleccionada no existe');
				F::redireccionar(ruta('usuario.comprasSinCalificar'));
			}

			$Compra->condicion('WHERE', 'id', '=', $id);
			$Compra->condicion('WHERE', 'usuario', '=', F::traerConectado());

			$compra = $Compra->traer(1);
			if(!@$compra['id']) {
				Flash::rojo('La compra seleccionada no existe');
				F::redireccionar(ruta('usuario.comprasSinCalificar'));
			}

			$Ponchado = new Ponchado();
			$Ponchado->condicion('WHERE', 'id', '=', $compra['ponchado']);
			$compra['ponchado'] = $Ponchado->traer(1);

			$fotos = [];
			if(@$compra['ponchado']['foto1']) {
				$fotos []= $compra['ponchado']['foto1'];
			}
			if(@$compra['ponchado']['foto2']) {
				$fotos []= $compra['ponchado']['foto2'];
			}
			if(@$compra['ponchado']['foto3']) {
				$fotos []= $compra['ponchado']['foto3'];
			}

			if(count($fotos) > 0) {
				$foto = $fotos[0];
			}

			$this->render('usuario/calificar', ['compra'=>$compra, 'foto'=>$foto]);
		}

		public function enviarCalificar($id)
		{
			$Compra = new Compra();
			
			if(!$Compra->verificar('id', $id)) {
				Flash::rojo('La compra seleccionada no existe');
				F::redireccionar(ruta('usuario.comprasSinCalificar'));
			}

			$Compra->condicion('WHERE', 'id', '=', $id);
			$Compra->condicion('WHERE', 'usuario', '=', F::traerConectado());

			$compra = $Compra->traer(1);
			if(!@$compra['id']) {
				Flash::rojo('La compra seleccionada no existe');
				F::redireccionar(ruta('usuario.comprasSinCalificar'));
			}

			$campo = F::cargador();

			if(!$campo['comentario'] || !$campo['voto']) {
				Flash::rojo('Faltan datos');
				F::redireccionar(ruta('usuario.calificar', ['id'=>$id]));
			}

			if($compra['comentario'] != '') {
				Flash::rojo('Ya calificaste esta compra');
				F::redireccionar(ruta('usuario.comprasSinCalificar'));
			}

			$Compra->condicion('WHERE', 'id', '=', $id);

			$Compra->valores['voto'] = $campo['voto'];
			$Compra->valores['comentario'] = F::html($campo['comentario']);

			if($Compra->actualizar()) {
				// Actualizar ponchado
				$Compra->condicion('WHERE', 'id', '=', $id);
				$compra = $Compra->traer(1);

				$Compra->condicion('WHERE', 'ponchado', '=', $compra['ponchado']);
				$compras = $Compra->traer();

				$Ponchado = new Ponchado();
				$Ponchado->condicion('WHERE', 'id', '=', $compra['ponchado']);
				if(count($compras) >= 10) {
					// Sumamos los puntajes
					$voto = 0;
					for ($i=0; $i < count($compras); $i++) { 
						$voto += $compras[$i]['voto'];
					}
					$Ponchado->valores['votos'] = $votos / count($compras);
				}
				$Ponchado->valores['compras'] = count($compras);
				$Ponchado->actualizar();

				Flash::verde('La compra fue calificada correctamente');
				F::redireccionar(ruta('usuario.compras'));	
			}
		}

		public function verCompra($id)
		{
			$this->render('usuario/ver_compra');
		}

		public function tickets()
		{
			$Ticket = new Ticket();
			$Ticket->condicion('WHERE', 'usuario', '=', F::traerConectado());
			$Ticket->condicion('WHERE', 'ticket', '=', 0);
			$Ticket->orden(['fecha_alta'], 'DESC');
			$tickets = $Ticket->traer();
			$this->render('usuario/tickets', ['tickets'=>$tickets]);
		}

		public function enviarTicket()
		{
			$campo = F::cargador();
			$r = $this->V->validar([
				'titulo'    => $campo['titulo'] . "|requerido|min_3|max_100",
				'estado'    => $campo['estado'] . "|requerido|min_1|max_1",
				'detalle'    => $campo['detalle'] . "|requerido|min_3|max_500",
				'captcha'   =>  $_POST['g-recaptcha-response'] . "|captcha"
			]);
			
			if($r) {
				Flash::rojo($r);
				F::redireccionar(ruta('usuario.tickets'));
			}

			$Ticket = new Ticket();
			$Ticket->valores['titulo'] = F::html($campo['titulo']);
			$Ticket->valores['estado'] = $campo['estado'];
			$Ticket->valores['detalle'] = F::html($campo['detalle']);
			$Ticket->valores['usuario'] = F::traerConectado();

			if($Ticket->insertar()) {
				Flash::verde('Ticket creado con exito!');
				F::redireccionar(ruta('usuario.tickets'));
			}
		}

		public function verTicket($id)
		{
			$Ticket = new Ticket();
			$Ticket->condicion('WHERE', 'id', '=', $id);
			$Ticket->condicion('WHERE', 'usuario', '=', F::traerConectado());
			if(count($Ticket->traer()) == 0) {
				F::redireccionar(ruta('usuario.tickets'));
			}
			$Ticket->condicion('WHERE', 'id', '=', $id);
			$Ticket->condicion('WHERE', 'usuario', '=', F::traerConectado());
			$ticket = $Ticket->traer(1);

			$Ticket->condicion('WHERE', 'ticket', '=', $id);
			$Ticket->orden(['fecha_alta'], 'DESC');
			$respuestas = $Ticket->traer(5,null,'pagina');
			$pagina = $Ticket->paginacion();

			$this->render('usuario/ver_ticket', ['ticket'=>$ticket, 'respuestas'=>$respuestas, 'pagina'=>$pagina]);
		}

		public function enviarMensajeTicket($id)
		{
			$campo = F::cargador();
			$r = $this->V->validar([
				'detalle'    => $campo['detalle'] . "|requerido|min_3|max_500",
				'captcha'   =>  $_POST['g-recaptcha-response'] . "|captcha"
			]);
			
			if($r) {
				Flash::rojo($r);
				F::redireccionar(ruta('usuario.verTicket', ['id'=>$id]));
			}
			$Ticket = new Ticket();
			$Ticket->condicion('WHERE', 'id', '=', $id);
			$Ticket->condicion('WHERE', 'usuario', '=', F::traerConectado());

			if(count($Ticket->traer()) == 0) {
				F::redireccionar(ruta('usuario.tickets'));
			}

			$Ticket->condicion('WHERE', 'id', '=', $id);
			$Ticket->valores['respondido'] = 0;
			$Ticket->actualizar();

			$Ticket->valores['detalle'] = F::html($campo['detalle']);
			$Ticket->valores['usuario'] = F::traerConectado();
			$Ticket->valores['ticket'] = $id;

			if($Ticket->insertar()) {
				Flash::verde('Respuesta enviada con exito!');
				F::redireccionar(ruta('usuario.verTicket', ['id'=>$id]));
			}
		}
	}