<?php namespace Conexion;
	/**
	* 
	*/
	class SQLSrv_ch
	{
		public $tabla;
		public $campos;
		public $condiciones;
		public $ordenes;
		public $limite;
		public $pagina_get;

		public $valores;

		private $conexion;

		function __construct($servidor, $db, $usuario, $clave)
		{

			$connectionInfo = [
				"Database"	=>	$db,
				"UID"		=>	$usuario,
				"PWD"		=>	$clave
			];

			$this->conexion = sqlsrv_connect( $servidor, $connectionInfo);
		}

		public function traer()
		{
			$valores = [];
			// Preparamos los campos a mostrar
			// Ejecutamos los PREPARE
			$sql = "SELECT {$this->retornarLimites()} {$this->retornarCampos('query')} FROM {$this->tabla} {$this->retornarCondiciones()} {$this->retornarOrdenes()}";

			for ($i=0; $i < count($this->condiciones); $i++) { 
				$valores []= $this->condiciones[$i][3];
			}

			$stmt = sqlsrv_query( $this->conexion, $sql, $valores);
			if( $stmt === false ) {
				die( print_r( sqlsrv_errors(), true));
			}

			$listo = [];

			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			    $listo []= $row;
			}

			sqlsrv_free_stmt( $stmt);

			return $listo;
		}

		public function insertar()
		{
			$valores = [];
			$valores_r = [];
			$pregunta = [];

			$tipo = "";
			$a = [];
			foreach ($this->valores as $key => $value) {
				$valores []= $key;
				$valores_r []= $value;
				$pregunta []= "?"; 

				${$this->valores[$key]} = $value;
				$a [] = &${$this->valores[$key]};
				$tipo .= "s";
			}
			$valores = implode(', ', $valores);
			$pregunta = implode(', ', $pregunta);
			$sql = "INSERT INTO {$this->tabla} ({$valores}) VALUES ({$pregunta}) ; SELECT SCOPE_IDENTITY()";


			$valores = [];
			foreach ($this->valores as $key => $value) {
				$valores []= $value;
			}

			$stmt = sqlsrv_query( $this->conexion, $sql, $valores);
			if( $stmt === false ) {
			     die( print_r( sqlsrv_errors(), true));
			} else {
				sqlsrv_next_result($stmt); 
				sqlsrv_fetch($stmt); 
				return sqlsrv_get_field($stmt, 0);
			} 
		}

		public function actualizar()
		{
			$valores = [];

			$tipo = "";
			$a = [];
			foreach ($this->valores as $key => $value) {
				$valores []= $key . ' = ' . '?';

				${$this->valores[$key]} = $value;
				$a [] = &${$this->valores[$key]};
				$tipo .= "s";
			}

			for ($i=0; $i < count($this->condiciones); $i++) { 
				/* 
					0 = WHERE
					1 = 'campo'
					2 = '=', '!='
					3 = valor
				*/
				${$this->condiciones[$i][1]} = $this->condiciones[$i][3];
				$a [] = &${$this->condiciones[$i][1]};
				$tipo .= "s";
			}
			$valores = implode(', ', $valores);
			$sql = "UPDATE {$this->tabla} SET {$valores} {$this->retornarCondiciones()}";

			$valores = [];
			foreach ($this->valores as $key => $value) {
				$valores []= $value;
			}
			for ($i=0; $i < count($this->condiciones); $i++) { 
				$valores []= $this->condiciones[$i][3];
			}

			$stmt = sqlsrv_query( $this->conexion, $sql, $valores);

			$rows_affected = sqlsrv_rows_affected( $stmt);
			if( $stmt === false ) {
				die( print_r( sqlsrv_errors(), true));
			} elseif( $rows_affected == -1) {
				return 0;
			} else {
			    return $rows_affected;
			}
		}

		public function eliminar()
		{
			$sql = "DELETE FROM {$this->tabla} {$this->retornarCondiciones()}";	
			for ($i=0; $i < count($this->condiciones); $i++) { 
				$valores []= $this->condiciones[$i][3];
			}
			$stmt = sqlsrv_query( $this->conexion, $sql, $valores);
			if( $stmt === false ) {
				die( print_r( sqlsrv_errors(), true));
			}
		}

		public function verificar($campo, $valor)
		{
			$sql = "SELECT {$campo} FROM {$this->tabla} WHERE {$campo} = '{$valor}'";
			$stmt = sqlsrv_query( $this->conexion, $sql, [], ["Scrollable" => SQLSRV_CURSOR_KEYSET]);
			$listo = [];

			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			    $listo []= $row;
			}

			return count($listo);
		}

		/*
		 |
		 |	Sub funciones
		 |
		 */
		
		public function retornarCampos($tipo = null)
		{
			return ($this->campos) ? implode(", ", $this->campos) : ' * ';
		}
		
		private function retornarCondiciones()
		{
			$sql = "";
			if(is_array($this->condiciones)) {
				/* 
					0 = WHERE
					1 = 'campo'
					2 = '=', '!='
					3 = valor
				*/
	    		for ($i=0; $i < count($this->condiciones); $i++) { 
	    			if($this->condiciones[$i][0] == 'WHERE') {

	    				if($i == 0) {
	    					$sql .= $this->condiciones[$i][0] . ' ' . $this->condiciones[$i][1] . ' ' . $this->condiciones[$i][2] . ' ?';
	    				} else {
	    					$sql .= ' AND ' . $this->condiciones[$i][1] . ' ' . $this->condiciones[$i][2] . ' ?';
	    				}
	    			}

	    			if($this->condiciones[$i][0] == 'OR') {
	    				$sql .= $this->condiciones[$i][0] . ' ' . $this->condiciones[$i][1] . ' ' . $this->condiciones[$i][2] . ' ?';
	    			}
	    		}
	    	}

	    	

	    	return $sql;
		}

		private function retornarOrdenes()
		{
			$sql = "";
			if($this->ordenes) {
				for ($i=0; $i < count($this->ordenes); $i++) { 
					$sql .= " ORDER BY " . implode(", ", $this->ordenes[$i][0]) ." ". $this->ordenes[$i][1];
				}
				return $sql;
			}
		}

		private function retornarLimites() {
			if(!$this->limite) {
				return null;
			}

			$sql = " TOP " . $this->limite . " ";	
			return $sql;
		}

		function __destruct()
        {
        	sqlsrv_close($this->conexion);
        	$this->conexion = null;
        }
	}
?>