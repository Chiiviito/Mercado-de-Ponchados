<?php
	namespace Sistema;

	class Seo
	{
		function __construct(){
			global $cfg;
			$this->sitio = $cfg['sitio'];
			$this->etiquetas = "";
			$this->titulo = "";
			$this->descripcion = "";
			$this->imagen = "";
			$this->ruta = $this->sitio['protocolo'] . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		}

		public function iniciar($necesita, $titulo = null, $descripcion = null, $imagen = null)
		{	
			$this->titulo = substr($titulo, 0, 60);
			if($necesita) {				
				$this->descripcion = substr($descripcion, 0, 155);
				$this->imagen = ($imagen) ?  $imagen : $this->ruta . $this->sitio['img_seo'];

				$this->etiquetas .= "<meta name=\"robots\" content=\"odp\"/>\n";
				$this->general();
				$this->googlePlus();
				$this->twitter();
				$this->openGraphdata();
			} else {
				$this->etiquetas .= "<title>" . $this->titulo . " - " . $this->sitio['nombre'] . "</title>\n";
				$this->etiquetas .= "<meta name=\"robots\" content=\"noodp\"/>\n";
				//this->etiquetas .= "<meta name=\"robots\" content=\"noodp\">";
			}

			return $this->etiquetas;
		}

		private function general()
		{
			$this->etiquetas .= "<title>" . $this->titulo . " - " . $this->sitio['nombre'] . "</title>\n";
			$this->etiquetas .= "<meta name=\"description\" content=\"" . $this->descripcion . "\" />\n";
		}

		private function googlePlus()
		{
			$this->etiquetas .= "<meta itemprop=\"name\" content=\"" . $this->titulo . "\">\n";
			$this->etiquetas .= "<meta itemprop=\"description\" content=\"" . $this->descripcion . "\">\n";
			$this->etiquetas .= "<meta itemprop=\"image\" content=\"" . $this->imagen . "\">\n";
		}

		private function twitter()
		{
			$this->etiquetas .= "<meta name=\"twitter:card\" value=\"summary\">\n";
			$this->etiquetas .= "<meta name=\"twitter:card\" content=\"" . $this->imagen . "\">\n";
			//$this->etiquetas .= "<meta name=\"twitter:site\" content=\"@publisher_handle\">\n";
			$this->etiquetas .= "<meta name=\"twitter:title\" content=\"" . $this->titulo . "\">\n";
			$this->etiquetas .= "<meta name=\"twitter:description\" content=\"" . $this->descripcion . "\">\n";
			//$this->etiquetas .= "<meta name=\"twitter:creator\" content=\"@author_handle\">\n";
			// witter summary card with large image must be at least 280x150px
			$this->etiquetas .= "<meta name=\"twitter:image:src\" content=\"" . $this->imagen . "\">\n";
		}

		private function openGraphdata()
		{
			$this->etiquetas .= "<meta property=\"og:title\" content=\"" . $this->titulo . "\" />\n";
			$this->etiquetas .= "<meta property=\"og:description\" content=\"" . $this->descripcion . "\" /> \n";
			$this->etiquetas .= "<meta property=\"og:image\" content=\"" . $this->imagen . "\" />\n";
			$this->etiquetas .= "<meta property=\"og:url\" content=\"" . $this->ruta . "\" />\n";
			$this->etiquetas .= "<meta property=\"og:type\" content=\"website\" />\n";
			$this->etiquetas .= "<meta property=\"og:site_name\" content=\"" . $this->sitio['nombre'] . "\" />\n";
			$this->etiquetas .= "<meta property=\"og:locale\" content=\"es_ES\" />\n";
			$this->etiquetas .= "<meta property=\"fb:app_id\" content=\"333216587152455\" />\n";
		}
	}
?>