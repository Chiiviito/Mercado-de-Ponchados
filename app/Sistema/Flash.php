<?php
	namespace Sistema;
	class Flash
	{
		public static function rojo($texto, $nombre = 'flash')
		{
			$_SESSION[$nombre] = "<div class='alert alert-danger'><i class='fa fa-times' aria-hidden='true'></i> {$texto}</div>";
		}
		public static function verde($texto, $nombre = 'flash')
		{
			$_SESSION[$nombre] = "<div class='alert alert-success'><i class='fa fa-check' aria-hidden='true'></i> {$texto}</div>";
		}
		public static function azul($texto, $nombre = 'flash')
		{
			$_SESSION[$nombre] = "<div class='alert alert-info'><i class='fa fa-info-circle' aria-hidden='true'></i> {$texto}</div>";
		}
	}
?>