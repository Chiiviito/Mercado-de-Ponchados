<?php namespace Sistema;
	class Validador {

		private $nombre;
		private $valor;

		function __construct(){
			global $idioma;
			$this->M = $idioma;
		}

		public function validar($a) {
			$resultado = true;
			$nombre = "";
			foreach ($a as $llave => $valor) {
				$v = explode("|", $valor);
				$this->nombre = $llave;
				for ($i=1; $i < count($v); $i++) { 
					if(is_string($resultado)) {
						break;
					} else {
						if($this->verificarM($v[$i])) {
							$m = explode("_", $v[$i]);
							if(method_exists(__CLASS__,$m[0])) {
								if($resultado == true) {
									$resultado = $this->$m[0]($v[0], $m[1]);
								}
							}
						} else {
							if(method_exists(__CLASS__,$v[$i])) {
								$resultado = $this->$v[$i]($v[0]);
							}
						}
					}
				}
				if(is_string($resultado)) { break; }
			}
			$resultado = (is_string($resultado)) ? $resultado : null;

			return $resultado;
		}

		private function verificarM($valor = NULL) {
			$m = substr($valor, 0, 3);
			if($m == 'min' || $m == 'max') {
				return true;
			} else {
				return false;
			}
		}

		private function requerido ($valor = NULL) {
			if(!empty($valor)) {
				return true;
			} else {
				return sprintf($this->M['requerido'], $this->nombre);				
			}
		}

		private function min ($valor = NULL, $longitud = NULL) {
			if(strlen($valor) < $longitud) {				 
				 return sprintf($this->M['minimo'], $this->nombre, $longitud);
			} else {
				return true;
			}
		}

		private function max ($valor = NULL, $longitud = NULL) {
			if(strlen($valor) > $longitud) {				 
				 return sprintf($this->M['maximo'], $this->nombre, $longitud);
			} else {
				return true;
			}
		}

		private function correo($valor = NULL) {
			if(!filter_var($valor, FILTER_VALIDATE_EMAIL)) {
				return sprintf($this->M['correo'], $this->nombre);
			} else {
				// Next check the domain is real.
				$domain = explode("@", $valor, 2);
				if(!checkdnsrr($domain[1])) {
					return sprintf($this->M['correo'], $this->nombre);
				}
			}
			return true;
		}

		public function alfanumerico($valor = NULL) {			
			if(!preg_match('/^([aA0-zZ9]{4,20})$/',$valor)) {
				return sprintf($this->M['alfanumerico'], $this->nombre);
			} else {
				return true;
			}
		}

		private function entero($valor = NULL) {
			if(is_int(intval($valor))) {
				return true;
			} else {
				return sprintf($this->M['entero'], $this->nombre);
			}
		}

		private function captcha($valor = NULL) {
			global $cfg;
			if($cfg['sistema']['dev']) {
				return true;
			}
			$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$cfg['captcha']['privado']}&response=".$valor);
	        $response = json_decode($response, true);

	        if(@$response["error-codes"]){
	        	return sprintf($this->M['captcha_incorrecto']);
	        } else {
	        	return true;
	        }
		}

		private function imagen($valor = NULL) {
			$imagesizedata = getimagesize($valor);
	        if ($imagesizedata === FALSE) {
	            return sprintf($this->M['no_es_imagen'], $this->nombre);
	        } else {
	            return true;
	        }
		}

		
		private function numerico($valor = NULL) {			
			if(!is_numeric($valor)) {
				return sprintf($this->M['numerico'], $this->nombre);
			} else {
				return true;
			}
		}

		private function hexColor($valor = NULL) {			
			if(!preg_match('/^([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})$/',$valor)) {
				return sprintf($this->M['hex'], $this->nombre);
			} else {
				return true;
			}
		}
	}
?>