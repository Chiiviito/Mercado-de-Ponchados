<?php
	namespace Sistema;

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	class Enviador
	{
		public function __construct($datos = []) {
			/*
			$datos = [
				'de' => ['correodelusuario@loquesea.com', 'Nombre Apellidos'],
				'para' => [['correodelusuario@loquesea.com', 'Nombre Apellidos'], ['correodelusuario@loquesea.com', 'Nombre Apellidos'], ['correodelusuario@loquesea.com', 'Nombre Apellidos']],
				'cc' => [['correodelusuario@loquesea.com', 'Nombre Apellidos'], ['correodelusuario@loquesea.com', 'Nombre Apellidos'], ['correodelusuario@loquesea.com', 'Nombre Apellidos']],
				'cco' => [['correodelusuario@loquesea.com', 'Nombre Apellidos'], ['correodelusuario@loquesea.com', 'Nombre Apellidos'], ['correodelusuario@loquesea.com', 'Nombre Apellidos']],
				'responder' => ['correodelusuario@loquesea.com', 'Nombre Apellidos'],
				'adjunto' => [['/tmp/image.jpg', 'new.jpg'], ['/tmp/image.jpg', 'new.jpg']],
				'titulo' => '',
				'mensajeHTML' => '',
				'mensajePlano' => ''
			];
			*/
			$this->mail = new PHPMailer(true);                              // Passing `true` enables exceptions
				// Server settings
				$this->mail->SMTPDebug = 2;                                 // Enable verbose debug output
				$this->mail->IsMail();                                      // Set mailer to use SMTP
				$this->mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
				$this->mail->SMTPAuth = false;                               // Enable SMTP authentication
				$this->mail->Username = 'robertocalani94@gmail.com';                 // SMTP username
				$this->mail->Password = 'lindavida10';                           // SMTP password
				$this->mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
				$this->mail->Port = 587;                                    // TCP port to connect to

				$this->mail->isHTML(true);                                  // Set email format to HTML
				
				/*
				// Recipients
				$mail->setFrom('from@example.com', 'Mailer');
				$mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
				$mail->addAddress('ellen@example.com');               // Name is optional
				$mail->addReplyTo('info@example.com', 'Information');
				$mail->addCC('cc@example.com');
				$mail->addBCC('bcc@example.com');

				// Attachments
				$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
				$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
				*/

				// Cargar datos

				// Para
				if(@$datos['para']) {
					for ($i=0; $i < count($datos['para']); $i++) { 
						$this->mail->addAddress($datos['para'][$i][0], ($datos['para'][$i][1]) ? $datos['para'][$i][1] : '');
					}
				}

				// CC
				if(@$datos['cc']) {
					for ($i=0; $i < count($datos['cc']); $i++) { 
						$this->mail->addAddress($datos['cc'][$i][0], ($datos['cc'][$i][1]) ? $datos['cc'][$i][1] : '');
					}
				}

				// CCO
				if(@$datos['cco']) {
					for ($i=0; $i < count($datos['cco']); $i++) { 
						$this->mail->addAddress($datos['cco'][$i][0], ($datos['cco'][$i][1]) ? $datos['cco'][$i][1] : '');
					}
				}

				// Responder
				if(@$datos['responder']) {
					$this->mail->addReplyTo($datos['responder'][0], ($datos['responder'][1]) ? $datos['responder'][1] : '');
				}

				$this->mail->Subject = $datos['titulo'];
				$this->leerHTML($datos['mensajeHTML']);
				$this->mail->AltBody = (@$datos['mensajePlano']) ?@ $datos['mensajePlano'] : '';
				$this->mail->setFrom($datos['de'][0], ($datos['de'][1]) ? $datos['de'][1] : '');

				//$this->mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

				//this->$mail->send();
		}

		public function leerHTML($mensaje = []) {
			$html = file_get_contents(variables('directorio') . '/vistas/correo/correo.html');
			$html = str_replace('%titulo%', $mensaje['titulo'], $html);
			$html = str_replace('%cuerpo%', $mensaje['cuerpo'], $html);
			$this->mail->Body = $html;

		}

		public function enviar()
		{
			return $this->mail->send();
		}

		function __destruct()
		{
			
		}
	}
?>