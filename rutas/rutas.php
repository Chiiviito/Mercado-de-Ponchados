<?php

	/**
	 * 
	 * Aca declaramos las rutas
	 * 
	 */
	$R = new Sistema\Ruta();
	$R->crearBase($cfg['sistema']['base']);

	$R->crearControlador('Controlador');

	/**
	 * 
	 * Rutas ADMIN
	 * 
	 */
		$R->agregarRuta('GET', '/admin', 'SitioControlador|admin', 'admin.index');
		$R->agregarRuta('POST', '/admin', 'SitioControlador|enviarIngresoAdmin', 'admin.enviarIngresoAdmin');
			
		$R->agregarRuta('GET', '/admin/ponchados', 'AdminControlador|ponchados', 'admin.ponchados');
		$R->agregarRuta('POST', '/admin/ponchados', 'AdminControlador|enviarPonchado', 'admin.enviarPonchado');

		$R->agregarRuta('GET', '/admin/categorias', 'AdminControlador|categorias', 'admin.categorias');
		$R->agregarRuta('POST', '/admin/categorias', 'AdminControlador|enviarCategoria', 'admin.enviarCategoria');

		$R->agregarRuta('GET', '/admin/paginas', 'AdminControlador|paginas', 'admin.paginas');
		$R->agregarRuta('POST', '/admin/paginas', 'AdminControlador|enviarPagina', 'admin.enviarPagina');

		$R->agregarRuta('GET', '/admin/compras', 'AdminControlador|compras', 'admin.compras');

		$R->agregarRuta('GET', '/admin/tickets', 'AdminControlador|tickets', 'admin.tickets');

		$R->agregarRuta('GET', '/admin/tickets/[i:id]', 'AdminControlador|verTicket', 'admin.verTicket');
		$R->agregarRuta('POST', '/admin/tickets/[i:id]', 'AdminControlador|enviarMensajeTicket', 'admin.enviarMensajeTicket');


		$R->agregarRuta('GET', '/admin/saldos', 'AdminControlador|saldos', 'admin.saldos');
		$R->agregarRuta('POST', '/admin/saldos', 'AdminControlador|enviarSaldo', 'admin.enviarSaldo');

		
	/**
	 * 
	 * Rutas Sitio
	 * 
	 */

		// Sitio
		$R->agregarRuta('GET',  '/', 'SitioControlador|index', 'sitio.index');
		
		$R->agregarRuta('GET',  '/registro', 'SitioControlador|registro', 'sitio.registro');
		$R->agregarRuta('POST',  '/registro', 'SitioControlador|enviarRegistro', 'sitio.enviarRegistro');

		$R->agregarRuta('GET',  '/ingresar', 'SitioControlador|ingresar', 'sitio.ingresar');
		$R->agregarRuta('POST',  '/ingresar', 'SitioControlador|enviarIngresar', 'sitio.enviarIngresar');

		$R->agregarRuta('GET',  '/recuperar', 'SitioControlador|recuperar', 'sitio.recuperar');
		$R->agregarRuta('POST',  '/recuperar', 'SitioControlador|enviarRecuperar', 'sitio.enviarRecuperar');

		$R->agregarRuta('GET',  '/ponchado/[i:id]/[*:titulo_url]', 'SitioControlador|ponchado', 'sitio.ponchado');
		$R->agregarRuta('GET',  '/ponchado/comprar/[i:id]/[*:titulo_url]', 'SitioControlador|comprar', 'sitio.comprar');
		$R->agregarRuta('POST',  '/ponchado/comprar/[i:id]/[*:titulo_url]', 'SitioControlador|enviarComprar', 'sitio.enviarComprar');
		$R->agregarRuta('GET',  '/descargar/[i:id]/tipo/[i:tipo]', 'SitioControlador|descargar', 'sitio.descargar');
		$R->agregarRuta('POST',  '/eliminar', 'SitioControlador|eliminar', 'sitio.eliminar');

		$R->agregarRuta('GET',  '/categoria/[i:id]/[*:nombre_url]', 'SitioControlador|categoria', 'sitio.categoria');
		$R->agregarRuta('GET',  '/buscador', 'SitioControlador|buscar', 'sitio.buscar');
		$R->agregarRuta('GET',  '/gratis', 'SitioControlador|gratis', 'sitio.gratis');

		$R->agregarRuta('GET',  '/p/[a:nombre]', 'SitioControlador|encontrarPagina', 'sitio.encontrarPagina');
		$R->agregarRuta('GET',  '/pagina/[a:nombre]/[*:titulo_url]', 'SitioControlador|pagina', 'sitio.pagina');

		$R->agregarRuta('GET',  '/re-validar', 'SitioControlador|reValidar', 'sitio.reValidar');
		$R->agregarRuta('POST',  '/re-validar', 'SitioControlador|enviarReValidar', 'sitio.enviarReValidar');
		$R->agregarRuta('GET',  '/validar/[*:codigo]', 'SitioControlador|validarUsuario', 'sitio.validarUsuario');

		$R->agregarRuta('GET',  '/boton-mp', 'SitioControlador|boton', 'sitio.boton');
		$R->agregarRuta('GET',  '/notification/success', 'SitioControlador|notification', 'sitio.notification');


		// Usuario
		$R->agregarRuta('GET',  '/usuario/datos', 'UsuarioControlador|datos', 'usuario.datos');
		$R->agregarRuta('POST',  '/usuario/datos', 'UsuarioControlador|enviarDatos', 'usuario.enviarDatos');

		$R->agregarRuta('GET',  '/usuario/cambiar-clave', 'UsuarioControlador|cambiarClave', 'usuario.cambiarClave');
		$R->agregarRuta('POST',  '/usuario/cambiar-clave', 'UsuarioControlador|enviarCambiarClave', 'usuario.enviarCambiarClave');

		$R->agregarRuta('GET',  '/usuario/compras', 'UsuarioControlador|compras', 'usuario.compras');
		$R->agregarRuta('GET',  '/usuario/compras-sin-calificar', 'UsuarioControlador|comprasSinCalificar', 'usuario.comprasSinCalificar');
		$R->agregarRuta('GET',  '/usuario/calificar/[i:id]', 'UsuarioControlador|calificar', 'usuario.calificar');
		$R->agregarRuta('GET',  '/cargar-saldo', 'UsuarioControlador|cargarSaldo', 'usuario.cargarSaldo');
		$R->agregarRuta('POST',  '/usuario/calificar/[i:id]', 'UsuarioControlador|enviarCalificar', 'usuario.enviarCalificar');
		$R->agregarRuta('GET',  '/historial-saldo', 'UsuarioControlador|historialSaldo', 'usuario.historialSaldo');

		$R->agregarRuta('GET',  '/tickets', 'UsuarioControlador|tickets', 'usuario.tickets');
		$R->agregarRuta('POST',  '/tickets', 'UsuarioControlador|enviarTicket', 'usuario.enviarTicket');

		$R->agregarRuta('GET',  '/ticket/[i:id]', 'UsuarioControlador|verTicket', 'usuario.verTicket');
		$R->agregarRuta('POST',  '/ticket/[i:id]', 'UsuarioControlador|enviarMensajeTicket', 'usuario.enviarMensajeTicket');





	/**
	 * 
	 * Acciones del SISTEMA
	 * 
	 */
	// Verifica si existe una ruta
	$R->agregarRuta('GET', '/api/verificar-ruta/[*:ruta]', 'SistemaControlador|verificarRuta', 'verificarRuta');	
	$R->agregarRuta('POST', '/api/ajax/accionar', 'SistemaControlador|verificarRuta2', 'verificarRuta2');	
	// 404
	$R->agregarRuta('GET', '/desconectar', 'SitioControlador|desconectar', 'desconectar');
	$R->agregarRuta('GET', '/[*:todo]', 'SitioControlador|p404', '404');
	$R->ejecutarMapeo();
