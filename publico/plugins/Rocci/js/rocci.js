$(function() {
	var url = document.location.toString();
	if (url.match('#')) {
		$('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
	} 

	// Change hash for page-reload
	$('.nav-tabs a').on('shown.bs.tab', function (e) {
		window.location.hash = e.target.hash;
	})
	
	$('.menu_card').prepend(function() {
		var atr = $(this).attr('ch-titulo');
		if (typeof atr === typeof undefined || atr === false) { return; }
		var datos = JSON.parse(atr);
		var icono = '';
		var titulo = '';

		icono = (datos.icono) ? '<i class="' + datos.icono + '"></i> ' : '';
		titulo = (datos.titulo) ? datos.titulo : 'Sin titulo';

		return '<li  class="titulo">' + icono + titulo + '</li>';
	})
	$('.b_card').prepend(function() {
		var atr = $(this).attr('ch-titulo');
		if (typeof atr === typeof undefined || atr === false) { return; }
		
		var datos = JSON.parse($(this).attr('ch-titulo'));

		var icono = '';
		var titulo = '';

		icono = (datos.icono) ? '<i class="' + datos.icono + '"></i> ' : '';
		titulo = (datos.titulo) ? datos.titulo : 'Sin titulo';
		return '<div class="t_card"><h5>' + icono + titulo + '</h5></div>';
	})
	$('[tooltip]').tooltip();
	$('select').prepend(function() {
		var placeholder = $(this).attr('placeholder');
		var valor = $(this).attr('value');
		if(placeholder && !valor) {
			$(this).css("color", "#999");
			$(this).find("option").css("color", "#333");            
			return '<option value="" disabled selected="selected">' + placeholder + '</option>';
		} else {
			/*var lista = $(this).find('option');
			for (var i = 0; i < lista.length; i++) {
				if(lista[i].text == valor) {
					
				}
			}*/
			$(this).val(valor);
		}
	});
	$('textarea').val(function() {
		var valor = $(this).attr('value');		
		//$(this).val(valor);
		if(valor) {
			return valor;
		}
	});
	$('select').change(function() {
		$(this).css("color", "#333");
	});
	$('select').css("padding", "5px");
});

function fName(nombre, valor) {
	if(!valor) {
		return $('[name=' + nombre + ']').val();
	} else {
		$('[name=' + nombre + ']').val(valor)
	}
}
function fId(id, valor) {
	if(!valor) {
		return $('#' + id).val();
	} else {
		$('#' + id).val(valor)
	}
}

/**
 * AJAX ----------------------------------------------------------------------------------------------------
 */

function rojax(datos) {
	
	if(!datos) {
		return;
	}

	if(!datos.tipo) {
		
	}
}

function fAjax(datos) {
	var estado = false;
	// Verificar ruta
	$.get("http://127.0.0.1/MercadoDeMatrices/api/verificar-ruta/" + datos.ruta, function(data, status){
		console.log(data);
		if(status == 200) {
			estado = data;
		}
	});

	console.log(estado);
	return;
	/*
		Toda ruta debe tener si es que lo necesite un datosRuta
	*/
	
	if(datos.post) {
		$.post("/MercadoDeMatrices/api/ajax/accionar",datos, function(data, status){
			var dataJSON = (data);
			console.log(dataJSON);
			alert("Dato: " + dataJSON + "\nEstado: " + status);
		});
	} else if(datos.get) {
		$.get("/MercadoDeMatrices/api/verificar-ruta/"+n, function(data, status){
	    	if(data == 1 && status == 200) {
	    		estado = true;
	    	}
	    });
	}
}